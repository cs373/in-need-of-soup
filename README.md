Estimated time of completion: 10 hours


Postman
https://documenter.getpostman.com/view/6781278/S11LsdCb#intro

Create a Tool that can be used to find a Soup Kitchen for people to visit, Donate to, Volunteer at based on Location. Should be able to take a location such as an address and find the nearest X amount of Soup Kitchens near you.
Possible expansion, Helping people get in contact with nearby fast food/ restaurants so that no excess food goes to waste (In the case if extra food was prepared but not used).
Possible Expansion. Being the itinerary manager / creating, managing, scheduling, and promoting Volunteer Work at a Soup Kitchen.
Possible Expansion: add photo of soup kitchen

API
1. Information about restaurant https://www.programmableweb.com/api/foodkit-restaurant
2. Ability to add scheduling: https://developers.google.com/calendar/
3. HERE, allow for text search on map: https://developer.here.com/documentation/places/topics/request-constructing.html
4. https://developers.google.com/maps/documentation/
5. https://developers.google.com/photos/library/guides/get-started


Page Layout
1. welcome page
	Pic and link to home page
2. home page
    	has sidebar with links to other pages (location, donations, volunteer, home ) and links on home
3. food pantries page
    	Hardcode name, addr, phone number, pic from organization, 
	also link to nutrition/volunteer/donation page, Search based on location?
3.1 food pantry info page (one or two)
	Info page for each individual soup kitchen that
4. nutrition page
    	Link to calorie counting page and link to medical diet needs page, possible pic of food? tbd

For the ultimate goal
4.1 calorie counting page
	Contains gov api info of calories, list common examples
4.2 medical diet needs
	Contains api info (hardcoded for this), give couple examples
5. volunteer page + donation
    	hardcode results from charity api, include name of org, addr, foundation description, organization description. Get location 
	General philosophy:
	for every valid organization (ability (optional) filter from charity api if accepting donations, give name, addr, foundation desc, organization description, deductibility, 
display contact info
7. About page
	a short description of the motivation for creating the website, pics of us, name, short description of us, gitlab stats, what we worked on, links to postman and gitlab

